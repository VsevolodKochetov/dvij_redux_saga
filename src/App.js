import React from 'react'
import {connect} from 'react-redux'
import './App.css'

// function* createGenerator() {
//   const f = yield 1
//   const s = yield f + 2
//   yield s + 3
// }

function App({ loading, data, getUsers }) {
  // const gen = createGenerator()
  // const genClick = () => {
  //   const obj = gen.next(2)
  //   if (!obj.done) console.info(obj.value)
  //   return
  // }
  return (
    <div className='App'>
      <button className='button' onClick={getUsers}>
        Get data
      </button>
      {/* <button className='button' onClick={genClick}>
        Generator
      </button> */}
      <div className='separator'></div>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <ul>
          {data.map(item => (
            <li key={item.id}>{`${item.name} : ${item.phone}`}</li>
          ))}
        </ul>
      )}
    </div>
  )
}

export default connect(
  state => ({ data: state.data, loading: state.loading }),
  dispatch => ({ getUsers: () => dispatch({ type: 'GET_USERS', payload: 'payload' }) })
)(App)
