import { call, put, takeLatest, all } from 'redux-saga/effects'

const getUsers = async () => {
  const response = await fetch('https://jsonplaceholder.typicode.com/users')
  return await response.json()
}

function* sagaWorker({payload}) {
  console.info(payload)
  try {
    yield put ({ type: 'USERS_REQUEST_START'})
    const users = yield call(getUsers)
    yield put ({ type: 'FILL_DATA', payload: users })
    yield put ({ type: 'USERS_REQUEST_SUCCESS'})
  } catch (e) {
    console.log(e)
  }
}

function* sagaWatcher() {
  yield takeLatest('GET_USERS', sagaWorker)
}

export default function* rootSaga() {
  yield all([
    sagaWatcher()
  ])
}
