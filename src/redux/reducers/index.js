const initialState = {
  data: [],
  loading: false
}

export const mainReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'USERS_REQUEST_START':
      return ({
        ...state, loading: true
      })
    case 'USERS_REQUEST_SUCCESS':
      return ({
        ...state, loading: false
      })
    case 'FILL_DATA':
      return ({
        data: action.payload
      })
    default: 
      return state  
  }
}